package console

import (
	"fmt"
)

type writer struct {
	isVerbose bool
}

func (writer *writer) info(msg string) {
	fmt.Println(msg)
}

func (writer *writer) verbose(msg string) {
	if writer.isVerbose {
		fmt.Println(msg)
	}
}
