package console

import (
	"time"

	"gitlab.com/slightlykiwi-oss/git-for/cmdline"
)

// Console holds the reader and writer objects
type Console struct {
	wrtr writer
}

// New creates a new instance of Console
func New(opts cmdline.Options) Console {
	newWriter := writer{opts.Verbose}
	return Console{newWriter}
}

// Info will print to the console regardless of the state of the verbose flag.
func (csl *Console) Info(msg string) {
	csl.wrtr.info(msg)
}

// Verbose will print to the console if the -v flag was set.
func (csl *Console) Verbose(msg string) {
	csl.wrtr.verbose(msg)
}

// TimeTrack measures elapsed time since start
func (csl *Console) TimeTrack(start time.Time, name string) {
	msg := timeTrack(start, name)
	csl.wrtr.info(msg)
}

// TimeTrackIfVerbose measures elapsed time since start but only prints it if the verbose flag is set
func (csl *Console) TimeTrackIfVerbose(start time.Time, name string) {
	msg := timeTrack(start, name)
	csl.wrtr.verbose(msg)
}
