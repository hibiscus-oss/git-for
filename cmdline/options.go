package cmdline

import (
	"fmt"
	"strings"
)

const verboseArg = "-v"

// Options represents the program options specified via command line arguments
type Options struct {
	Template string
	Verbose  bool
}

// ReadArguments reads the command line arguments into a an Options struct
func ReadArguments(args []string) (Options, error) {
	argLen := len(args)

	newOpts := Options{}
	if argLen < 1 || strings.HasPrefix(args[0], "-") {
		return newOpts, fmt.Errorf("Missing template argument")
	}

	newOpts.Template = args[0]

	for i := 1; i < argLen; i++ {
		arg := args[i]
		switch arg {
		case "-v":
			newOpts.Verbose = true
			break
		default:
			return newOpts, fmt.Errorf("Unknown command line argument: " + arg)
		}
	}

	return newOpts, nil
}
