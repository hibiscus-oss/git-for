package project

import (
	"os/exec"
	"reflect"
	"testing"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

func TestFormatCommands(t *testing.T) {
	type test struct {
		name    string
		cfg     configurator
		configs []Config
		want    []*exec.Cmd
		wantErr bool
	}

	mockCfg := configurator{nil, nil, console.Console{}}

	mockCommand1 := exec.Command("git", "config", "test.test", "true")
	mockCommand2 := exec.Command("git", "config", "test.testy.test", "testval2")

	tests := []test{
		test{"TwoCommands", mockCfg, []Config{Config{"test.test", "true"}, Config{"test.testy.test", "testval2"}}, []*exec.Cmd{mockCommand1, mockCommand2}, false},
		test{"NoCommands", mockCfg, []Config{}, []*exec.Cmd{}, false},
		test{"MalformedCommand", mockCfg, []Config{Config{"", ""}}, []*exec.Cmd{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.cfg.formatCommands(tt.configs)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("configurator.formatCommands() = %v, want %v", got, tt.want)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Reader.findTemplateFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
