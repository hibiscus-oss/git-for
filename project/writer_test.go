package project

import (
	"testing"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

const mockWd = "M:/mockdir"

func TestGetFileInWd(t *testing.T) {
	type test struct {
		name    string
		wrtr    writer
		file    string
		want    string
		wantErr bool
	}

	mockWriter := writer{nil, mockGetWorkingDir, nil, console.Console{}}

	tests := []test{
		test{"GitIgnore", mockWriter, gitIgnoreFile, mockWd + "/" + gitIgnoreFile, false},
		test{"GitConfig", mockWriter, gitConfigFile, mockWd + "/" + gitConfigFile, false},
		test{"GitAttributes", mockWriter, gitAttributesFile, mockWd + "/" + gitAttributesFile, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.wrtr.getFileInWd(tt.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("writer.getFileInWd() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("writer.getFileInWd() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMergeLines(t *testing.T) {
	type params struct {
		existing string
		new      []string
	}

	type test struct {
		name    string
		wrtr    writer
		params  params
		want    string
		wantErr bool
	}

	mockWriter := writer{nil, nil, nil, console.Console{}}
	existing := "*.pop\n\\loremipsum\n*.test2\n##DolorSitAmet\n\n\n*.test1"
	new := []string{"*.test1", "*.test2", "*.test3"}
	expected := appliedHeader + "*.test1\n*.test2\n*.test3"
	expectedMerged := "*.pop\n\\loremipsum\n##DolorSitAmet\n\n\n" + expected

	tests := []test{
		test{"Blank", mockWriter, params{"", new}, expected, false},
		test{"NotBlank", mockWriter, params{existing, new}, expectedMerged, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.wrtr.mergeLines(tt.params.existing, tt.params.new)
			if (err != nil) != tt.wantErr {
				t.Errorf("writer.getFileInWd() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("writer.getFileInWd() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRemoveStrings(t *testing.T) {
	type args struct {
		current string
		lines   []string
	}

	type test struct {
		name string
		wrtr writer
		args args
		want string
	}

	mockWriter := writer{nil, nil, nil, console.Console{}}

	tests := []test{
		test{"Default", mockWriter, args{"Lorem\nIpsum\nDolor\nSit\nAmet\n", []string{"Amet", "Ipsum", "Dolor"}}, "Lorem\nSit\n"},
		test{"HardString", mockWriter, args{"Lorem\nIpsumLorem\nLoremIpsum\nIpsum\nDolor\nSit\nAmet\n", []string{"Amet", "Ipsum", "Dolor"}}, "Lorem\nIpsumLorem\nLoremIpsum\nSit\n"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.wrtr.removeLines(&tt.args.current, tt.args.lines); tt.args.current != tt.want {
				t.Errorf("removeLines() = %v, want %v", tt.args.current, tt.want)
			}
		})
	}
}

func mockFileExists(path string) bool {
	return true
}

func mockFileNotExists(path string) bool {
	return false
}

func mockGetWorkingDir() (string, error) {
	return mockWd, nil
}
