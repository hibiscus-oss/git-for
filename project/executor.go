package project

import (
	"fmt"
	"os/exec"
	"strings"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

// Executor exposes methods for executing a series of commands
type Executor interface {
	Execute(cmds []string) error
}

// configurator holds mockable methods and objects
type executor struct {
	fileExists func(path string) bool
	runCommand func(*exec.Cmd) (string, string, error)
	csl        console.Console
}

// createConfigurator constructs a new instance of configurator with actual methods
func createExecutor(csl console.Console) executor {
	return executor{fileExists, runCommand, csl}
}

func (exe executor) Execute(commands []string) error {
	if !exe.fileExists(".git") {
		return fmt.Errorf("Not in the root of a git repository. Please call from the root of your repo")
	}

	cmds, err := exe.formatCommands(commands)
	if err != nil {
		return err
	}

	for _, command := range cmds {
		exe.csl.Verbose("Running " + command.Path + " " + strings.Join(command.Args, " "))

		outbuf, errbuf, err := exe.runCommand(command)
		if err != nil {
			return err
		}

		if outbuf != "" {
			exe.csl.Verbose("Output:\n" + outbuf)
		}
		if errbuf != "" {
			exe.csl.Verbose("Errors:\n" + errbuf)
		}
	}

	return nil
}

func (exe executor) formatCommands(commands []string) ([]*exec.Cmd, error) {
	result := []*exec.Cmd{}

	for _, command := range commands {
		commandElements := strings.Split(command, " ")
		if len(commandElements) > 0 {
			cmdExec := commandElements[0]
			cmd := exec.Command(cmdExec)
			cmd.Args = commandElements
			result = append(result, cmd)
		}
	}

	return result, nil
}
