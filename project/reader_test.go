package project

import (
	"reflect"
	"testing"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

func TestIntegration(t *testing.T) {
	type test struct {
		name     string
		rdr      reader
		filename string
		want     Template
		wantErr  bool
	}

	expectedIgnores := []string{"*.lol", "*.test"}
	expectedConfigs := []Config{Config{"merge.tool", "unityyamlmerge"}}
	expectedAttributes := []string{"*.php text", "*.css text"}
	expectedCommands := []string{"git nothing", "git dusty"}

	expectedDefault := Template{expectedIgnores, expectedConfigs, expectedAttributes, expectedCommands}

	mockReader := reader{mockBuiltinFileExists, mockGetEnvVar, mockDefaultFile, console.Console{}}

	tests := []test{
		test{"DefaultFile", mockReader, "test", expectedDefault, false},
		test{"FileNotFound", mockReader, "banana", Template{}, true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.rdr.Read(tt.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("Reader.Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reader.Read() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRead(t *testing.T) {
	type test struct {
		name    string
		rdr     reader
		want    Template
		wantErr bool
	}

	expectedIgnores := []string{"*.lol", "*.test"}
	expectedConfigs := []Config{Config{"merge.tool", "unityyamlmerge"}}
	expectedAttributes := []string{"*.php text", "*.css text"}
	expectedCommands := []string{"git nothing", "git dusty"}

	expectedDefault := Template{expectedIgnores, expectedConfigs, expectedAttributes, expectedCommands}
	expectedIgnoresOnly := Template{}
	expectedIgnoresOnly.Ignores = expectedIgnores

	expectedConfigsOnly := Template{}
	expectedConfigsOnly.Configs = expectedConfigs

	expectedAttributesOnly := Template{}
	expectedAttributesOnly.Attributes = expectedAttributes

	expectedCommandsOnly := Template{}
	expectedCommandsOnly.Commands = expectedCommands

	mockConsole := console.Console{}

	tests := []test{
		test{"EmptyFile", reader{nil, nil, mockEmptyFile, mockConsole}, Template{}, true},
		test{"DefaultFile", reader{nil, nil, mockDefaultFile, mockConsole}, expectedDefault, false},
		test{"IgnoresOnly", reader{nil, nil, mockIgnoresOnly, mockConsole}, expectedIgnoresOnly, false},
		test{"ConfigsOnly", reader{nil, nil, mockConfigsOnly, mockConsole}, expectedConfigsOnly, false},
		test{"AttributesOnly", reader{nil, nil, mockAttributesOnly, mockConsole}, expectedAttributesOnly, false},
		test{"CommandsOnly", reader{nil, nil, mockCommandsOnly, mockConsole}, expectedCommandsOnly, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.rdr.read("test")
			if (err != nil) != tt.wantErr {
				t.Errorf("Reader.read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reader.read() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFindFile(t *testing.T) {
	type test struct {
		name     string
		rdr      reader
		filename string
		want     string
		wantErr  bool
	}

	mockConsole := console.Console{}
	mockBuiltInReader := reader{mockBuiltinFileExists, mockGetEnvVar, nil, mockConsole}
	mockCustomReader := reader{mockCustomFileExists, mockGetEnvVar, nil, mockConsole}

	tests := []test{
		test{"FileNotExist", mockBuiltInReader, "bottle", "", true},
		test{"FileInBuiltin", mockBuiltInReader, "test", "<mockgopath>" + builtInDir + "test.json", false},
		test{"FileInCustom", mockCustomReader, "test", "<mockgopath>" + customDir + "test.json", false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.rdr.findTemplateFile(tt.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("Reader.findTemplateFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Reader.findTemplateFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func mockBuiltinFileExists(path string) bool {
	return path == "<mockgopath>"+builtInDir+"test.json"
}

func mockCustomFileExists(path string) bool {
	return path == "<mockgopath>"+customDir+"test.json"
}

func mockGetEnvVar(key string) string {
	return "<mockgopath>"
}

func mockDefaultFile(path string) ([]byte, error) {
	return []byte(`{"ignores":["*.lol","*.test"],` +
		`"configs":[{"key":"merge.tool","value":"unityyamlmerge"}],` +
		`"attributes":["*.php text","*.css text"],` +
		`"commands":["git nothing","git dusty"]}`), nil
}

func mockEmptyFile(path string) ([]byte, error) {
	return []byte(``), nil
}

func mockIgnoresOnly(path string) ([]byte, error) {
	return []byte(`{"ignores":["*.lol","*.test"]}`), nil
}

func mockConfigsOnly(path string) ([]byte, error) {
	return []byte(`{"configs":[{"key":"merge.tool", "value":"unityyamlmerge"}]}`), nil
}

func mockAttributesOnly(path string) ([]byte, error) {
	return []byte(`{"attributes":["*.php text","*.css text"]}`), nil
}

func mockCommandsOnly(path string) ([]byte, error) {
	return []byte(`{"commands":["git nothing","git dusty"]}`), nil
}
