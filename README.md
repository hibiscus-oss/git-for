# Automating repository configuration #

Git For is a simple, fast alternative to manually configuring your git repositories.

* Create .gitignore, .gitattributes and .git/config with one command
* Use built-in templates or create your own!
* Preserve the content of your files, git-for seamlessly merges the template into
your files.

# WARNING #

This is a pre-alpha build. I'm fairly certain it won't break things, but it might.
Also the API surface will probably undergo radical changes every few builds until we
hit beta. Expect your custom templates to break often at this stage.

# Usage #

1. Clone or init your repository
1. Execute git for TEMPLATENAME

That's it!

# Custom Templates #

## WARNING 
The API surface will probably undergo radical changes every few builds 
until we hit beta. Expect your custom templates to break often.

Templates are simply .json files.
Please place any custom templates in %GITFOR%\Custom

Refer to builtin templates for examples.

Note: You should use unique names for your custom templates as git-for prioritizes the
builtin directory over the custom directory.